import {Component} from '@angular/core';

import {Router, ActivatedRoute} from '@angular/router';

import {UserService} from '../shared/services/user.service';

@Component({
    moduleId: module.id,
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent {

    constructor(private userService: UserService, private router: Router, private route: ActivatedRoute){

    }

    name: string;
    age: number;
    gender: string;
    job: string;
    password: string;

    addUser(){

        if(this.name && this.age && this.gender && this.job && this.password){

            this.userService.addUser(this.name, this.age, this.gender, this.job, this.password);

            this.router.navigate(['../'], {relativeTo: this.route});

        }

    }

}