import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HTTP_PROVIDERS} from '@angular/http';

import {SharedModule} from '../shared/shared.module';
import {Level1Module} from './level1/level1.module';
import {Level2Module} from './level2/level2.module';
import {Level3Module} from './level3/level3.module';

import {VaultEntranceComponent, TerminalComponent, LoginComponent, RegisterComponent, UserListComponent, UpdateUserComponent, TerminalHomeComponent} from './index';

import {LoginService} from '../shared/services/login.service';
import {UserService} from '../shared/services/user.service';
import {GuardLevel1Service} from '../shared/guards/guard-level1.service';
import {GuardLevel2Service} from '../shared/guards/guard-level2.service';
import {GuardLevel3Service} from '../shared/guards/guard-level3.service';
import {GuardUpdateUserService} from '../shared/guards/guard-update-user.service';
import {GuardLoginService} from '../shared/guards/guard-login.service';

import {routing} from './vault.routing';

@NgModule({
    imports: [SharedModule, FormsModule, Level1Module, Level2Module, Level3Module, routing],
    declarations: [VaultEntranceComponent, TerminalComponent, LoginComponent, RegisterComponent, UserListComponent, UpdateUserComponent, TerminalHomeComponent],
    providers: [LoginService, UserService, GuardLevel1Service, GuardLevel2Service, GuardLevel3Service, GuardUpdateUserService, GuardLoginService, HTTP_PROVIDERS]
})
export class VaultModule {

}