import {Component, OnInit} from '@angular/core';

import {Router, ActivatedRoute} from '@angular/router';

import {UserService} from '../shared/services/user.service';

@Component({
    moduleId: module.id,
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit{

    constructor(private userService: UserService, private router: Router, private route: ActivatedRoute){

    }

    ngOnInit(){
        this.userService.getUsers();
    }

    //todo, navigate to logincomponent with name as parameter
    loginAsUser(id: number){
        this.router.navigate(['login', id], {relativeTo: this.route});
    }

}