"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var login_service_1 = require('../shared/services/login.service');
var user_service_1 = require('../shared/services/user.service');
var TerminalHomeComponent = (function () {
    function TerminalHomeComponent(router, route, loginService, userService) {
        this.router = router;
        this.route = route;
        this.loginService = loginService;
        this.userService = userService;
    }
    TerminalHomeComponent.prototype.removeUser = function () {
        var _this = this;
        this.userService.removeUser(this.loginService.getUser().getId()).then(function () {
            _this.router.navigate(['../../'], { relativeTo: _this.route });
        });
    };
    TerminalHomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: './terminal-home.component.html',
            styleUrls: ['./terminal-home.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, login_service_1.LoginService, user_service_1.UserService])
    ], TerminalHomeComponent);
    return TerminalHomeComponent;
}());
exports.TerminalHomeComponent = TerminalHomeComponent;
//# sourceMappingURL=terminal-home.component.js.map