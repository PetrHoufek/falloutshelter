import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {VaultEntranceComponent, TerminalComponent, LoginComponent, RegisterComponent, UserListComponent, UpdateUserComponent, TerminalHomeComponent} from './index';

import {GuardLevel1Service} from '../shared/guards/guard-level1.service';
import {GuardUpdateUserService} from '../shared/guards/guard-update-user.service';

export const vaultRoutes: Routes = [
    {
        path: '',
        redirectTo: 'entrance',
        pathMatch: 'full'
    },
    {
        path: 'entrance',
        children: [
            {
                path: '',
                component: VaultEntranceComponent,
            },
            {
                path: 'terminal',
                //If you want to display context of a component and under its <router-outlet></router-outlet> tag display the content of its children, you HAVE TO include
                //the "component" property in its path, using a componentless route and specifying the component as its child won't work!
                //If you ommited component property in the following line and instead put it in the first child path(while removing the redirectTo as well) it woudn't work 
                component: TerminalComponent,
                children: [ 
                    {
                        path: '',
                        component: TerminalHomeComponent
                    },
                    {
                        path: 'user-list',
                        children: [
                            {
                                path: '',
                                component: UserListComponent
                            },
                            {
                                path: 'login/:id',
                                component: LoginComponent
                            }
                        ]
                    },
                    {
                        path: 'register',
                        component: RegisterComponent
                    },
                    {
                        path: 'modify',
                        canActivate: [GuardLevel1Service],
                        canDeactivate: [GuardUpdateUserService],
                        component: UpdateUserComponent
                    }
                ]
            }
        ]

            //level 1
            //living-quarters
            //diner
            //lounge

            //level 2
            //weight-room
            //athletics-room
            //fitness-room
            //game-room
            //barbershop
            //classroom
            //radio-studio

            //level 3
            //outfit-workshop
            //power-generator
            //water-treatment
            //medbay
            //science-lab
            //weapon-workshop
            //garden
            //overseer-office

            //level 4
            //storage-room
            //armory
            //water-purification

            //level 5
            //nuclear-reactor

            //strength
            //perception
            //endurance
            //charisma
            //intelligence
            //agility
            //luck

    }
]

export const routing: ModuleWithProviders = RouterModule.forChild(vaultRoutes);