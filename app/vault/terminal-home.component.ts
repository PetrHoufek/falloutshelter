import {Component} from '@angular/core';

import {Router, ActivatedRoute} from '@angular/router';

import {LoginService} from '../shared/services/login.service';
import {UserService} from '../shared/services/user.service';


@Component({
    moduleId: module.id,
    templateUrl: './terminal-home.component.html',
    styleUrls: ['./terminal-home.component.css']
})
export class TerminalHomeComponent {

    constructor(private router: Router, private route: ActivatedRoute, private loginService: LoginService, private userService: UserService){

    }

    removeUser(){
        this.userService.removeUser(this.loginService.getUser().getId()).then(() => {
            this.router.navigate(['../../'], {relativeTo: this.route});
        });
    }

}