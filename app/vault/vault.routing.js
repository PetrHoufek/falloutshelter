"use strict";
var router_1 = require('@angular/router');
var index_1 = require('./index');
var guard_level1_service_1 = require('../shared/guards/guard-level1.service');
var guard_update_user_service_1 = require('../shared/guards/guard-update-user.service');
exports.vaultRoutes = [
    {
        path: '',
        redirectTo: 'entrance',
        pathMatch: 'full'
    },
    {
        path: 'entrance',
        children: [
            {
                path: '',
                component: index_1.VaultEntranceComponent,
            },
            {
                path: 'terminal',
                //If you want to display context of a component and under its <router-outlet></router-outlet> tag display the content of its children, you HAVE TO include
                //the "component" property in its path, using a componentless route and specifying the component as its child won't work!
                //If you ommited component property in the following line and instead put it in the first child path(while removing the redirectTo as well) it woudn't work 
                component: index_1.TerminalComponent,
                children: [
                    {
                        path: '',
                        component: index_1.TerminalHomeComponent
                    },
                    {
                        path: 'user-list',
                        children: [
                            {
                                path: '',
                                component: index_1.UserListComponent
                            },
                            {
                                path: 'login/:id',
                                component: index_1.LoginComponent
                            }
                        ]
                    },
                    {
                        path: 'register',
                        component: index_1.RegisterComponent
                    },
                    {
                        path: 'modify',
                        canActivate: [guard_level1_service_1.GuardLevel1Service],
                        canDeactivate: [guard_update_user_service_1.GuardUpdateUserService],
                        component: index_1.UpdateUserComponent
                    }
                ]
            }
        ]
    }
];
exports.routing = router_1.RouterModule.forChild(exports.vaultRoutes);
//# sourceMappingURL=vault.routing.js.map