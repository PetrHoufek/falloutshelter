"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Level3Component = (function () {
    function Level3Component() {
    }
    Level3Component = __decorate([
        core_1.Component({
            template: "\n    <div>\n        <div style=\"margin: 5px 0\">\n            Level 3\n            <button routerLink=\"../level2\">Up</button>\n            <button>Down</button>\n        </div>\n        <div>\n            <button routerLink=\"power-generator\">Power generator</button>\n            <button routerLink=\"water-treatment\">Water treatment</button>\n            <button routerLink=\"garden\">Garden</button>\n            <button routerLink=\"outfit-workshop\">Outfit workshop</button>\n            <button routerLink=\"weapon-workshop\">Weapon workshop</button>\n            <button routerLink=\"science-lab\">Science lab</button>\n            <button routerLink=\"medbay\">Medbay</button>\n            <button routerLink=\"overseer-office\">Overseer's office</button>\n        </div>\n        <router-outlet></router-outlet>\n    </div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], Level3Component);
    return Level3Component;
}());
exports.Level3Component = Level3Component;
//# sourceMappingURL=level3.component.js.map