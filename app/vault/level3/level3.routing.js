"use strict";
var router_1 = require('@angular/router');
var guard_level3_service_1 = require('../../shared/guards/guard-level3.service');
var index_1 = require('./index');
exports.levelRoutes = [
    {
        path: 'level3',
        component: index_1.Level3Component,
        canActivate: [guard_level3_service_1.GuardLevel3Service],
        canActivateChild: [guard_level3_service_1.GuardLevel3Service],
        children: [
            {
                path: '',
                redirectTo: 'lockers'
            },
            {
                path: 'lockers',
                component: index_1.LockersComponent
            },
            {
                path: 'power-generator',
                component: index_1.PowerGeneratorComponent
            },
            {
                path: 'water-treatment',
                component: index_1.WaterTreatmentComponent
            },
            {
                path: 'garden',
                component: index_1.GardenComponent
            },
            {
                path: 'outfit-workshop',
                component: index_1.OutfitWorkshopComponent
            },
            {
                path: 'weapon-workshop',
                component: index_1.WeaponWorkshopComponent
            },
            {
                path: 'science-lab',
                component: index_1.ScienceLabComponent
            },
            {
                path: 'medbay',
                component: index_1.MedbayComponent
            },
            {
                path: 'overseer-office',
                component: index_1.OverseerOfficeComponent
            }
        ]
    }
];
exports.routing = router_1.RouterModule.forChild(exports.levelRoutes);
//# sourceMappingURL=level3.routing.js.map