import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';

import {routing} from './level3.routing';

import {GardenComponent, Level3Component, MedbayComponent, OutfitWorkshopComponent, OverseerOfficeComponent, 
    PowerGeneratorComponent, ScienceLabComponent, WaterTreatmentComponent, WeaponWorkshopComponent, LockersComponent} from './index';

@NgModule({
    imports: [SharedModule, routing],
    declarations: [GardenComponent, Level3Component, MedbayComponent, OutfitWorkshopComponent, OverseerOfficeComponent, 
    PowerGeneratorComponent, ScienceLabComponent, WaterTreatmentComponent, WeaponWorkshopComponent, LockersComponent]
})
export class Level3Module {

}