import {Component} from '@angular/core';

@Component({
    template: 
    `
    <div>
        <div style="margin: 5px 0">
            Level 3
            <button routerLink="../level2">Up</button>
            <button>Down</button>
        </div>
        <div>
            <button routerLink="power-generator">Power generator</button>
            <button routerLink="water-treatment">Water treatment</button>
            <button routerLink="garden">Garden</button>
            <button routerLink="outfit-workshop">Outfit workshop</button>
            <button routerLink="weapon-workshop">Weapon workshop</button>
            <button routerLink="science-lab">Science lab</button>
            <button routerLink="medbay">Medbay</button>
            <button routerLink="overseer-office">Overseer's office</button>
        </div>
        <router-outlet></router-outlet>
    </div>
    `
})
export class Level3Component {
    
}