import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {GuardLevel3Service} from '../../shared/guards/guard-level3.service';

import {GardenComponent, Level3Component, MedbayComponent, OutfitWorkshopComponent, OverseerOfficeComponent, 
    PowerGeneratorComponent, ScienceLabComponent, WaterTreatmentComponent, WeaponWorkshopComponent, LockersComponent} from './index';

export const levelRoutes: Routes = [
    {
        path: 'level3',
        component: Level3Component,
        canActivate: [GuardLevel3Service],
        canActivateChild: [GuardLevel3Service],
        children: [
            {
                path: '',
                redirectTo: 'lockers'
            },
            {
                path: 'lockers',
                component: LockersComponent
            },
            {
                path: 'power-generator',
                component:PowerGeneratorComponent
            },
            {
                path: 'water-treatment',
                component:WaterTreatmentComponent
            },
            {
                path: 'garden',
                component:GardenComponent
            },
            {
                path: 'outfit-workshop',
                component:OutfitWorkshopComponent
            },
            {
                path: 'weapon-workshop',
                component:WeaponWorkshopComponent
            },
            {
                path: 'science-lab',
                component:ScienceLabComponent
            },
            {
                path: 'medbay',
                component: MedbayComponent
            },
            {
                path: 'overseer-office',
                component: OverseerOfficeComponent
            }
        ]
    }
]

export const routing: ModuleWithProviders = RouterModule.forChild(levelRoutes);