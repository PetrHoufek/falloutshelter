"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shared_module_1 = require('../../shared/shared.module');
var level3_routing_1 = require('./level3.routing');
var index_1 = require('./index');
var Level3Module = (function () {
    function Level3Module() {
    }
    Level3Module = __decorate([
        core_1.NgModule({
            imports: [shared_module_1.SharedModule, level3_routing_1.routing],
            declarations: [index_1.GardenComponent, index_1.Level3Component, index_1.MedbayComponent, index_1.OutfitWorkshopComponent, index_1.OverseerOfficeComponent,
                index_1.PowerGeneratorComponent, index_1.ScienceLabComponent, index_1.WaterTreatmentComponent, index_1.WeaponWorkshopComponent, index_1.LockersComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], Level3Module);
    return Level3Module;
}());
exports.Level3Module = Level3Module;
//# sourceMappingURL=level3.module.js.map