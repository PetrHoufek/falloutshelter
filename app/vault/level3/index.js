"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./level3.component'));
__export(require('./lockers.component'));
__export(require('./garden.component'));
__export(require('./medbay.component'));
__export(require('./outfit-workshop.component'));
__export(require('./overseer-office.component'));
__export(require('./power-generator.component'));
__export(require('./science-lab.component'));
__export(require('./water-treatment.component'));
__export(require('./weapon-workshop.component'));
//# sourceMappingURL=index.js.map