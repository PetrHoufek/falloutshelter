import {Component, OnInit} from '@angular/core';

import {LoginService} from '../shared/services/login.service';
import {UserService, User} from '../shared/services/user.service';

@Component({
    moduleId: module.id,
    templateUrl: './update-user.component.html',
    styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

    user: User;
    name: string;
    age: number;
    job: string;
    password: string;

    constructor(private loginService: LoginService, private userService: UserService){

    }

    ngOnInit(){

        this.user = this.loginService.getUser();
        this.name = this.user.getName();
        this.age = this.user.getAge();
        this.job = this.user.getJob();
        this.password = this.user.getPassword();

    }

    updateUser(){
        this.userService.updateUser(this.age, this.job, this.password, this.user.getId());
    }

    hasPendingChanges(): boolean{
        
        if(this.age === this.loginService.getUser().getAge() && this.job === this.loginService.getUser().getJob() && this.password === this.loginService.getUser().getPassword()){
            return false;
        }else{
            return true;
        }

    }

}