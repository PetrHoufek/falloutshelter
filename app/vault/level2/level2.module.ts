import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module'

import {Level2Component, AthleticsRoomComponent, BarbershopComponent, ClassroomComponent, FitnessRoomComponent, GameRoomComponent, RadioStudioComponent, WeightRoomComponent} from './index';

import {routing} from './level2.routing';

@NgModule({
    imports: [SharedModule, routing],
    declarations: [Level2Component, AthleticsRoomComponent, BarbershopComponent, ClassroomComponent, FitnessRoomComponent, GameRoomComponent, RadioStudioComponent, WeightRoomComponent]
})
export class Level2Module {

}