"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./level2.component'));
__export(require('./athletics-room.component'));
__export(require('./barbershop.component'));
__export(require('./classroom.component'));
__export(require('./fitness-room.component'));
__export(require('./game-room.component'));
__export(require('./radio-studio.component'));
__export(require('./weight-room.component'));
//# sourceMappingURL=index.js.map