"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Level2Component = (function () {
    function Level2Component() {
    }
    Level2Component = __decorate([
        core_1.Component({
            template: "\n    <div>\n        <div style=\"margin: 5px 0\">\n            Level 2\n            <button routerLink=\"../level1\">Up</button>\n            <button routerLink=\"../level3\">Down</button>\n        </div>\n        <div>\n            <button routerLink=\"athletics-room\">Athletics room</button>\n            <button routerLink=\"barbershop\">Barbershop</button>\n            <button routerLink=\"classroom\">Classroom</button>\n            <button routerLink=\"fitness-room\">Fitness room</button>\n            <button routerLink=\"game-room\">Game room</button>\n            <button routerLink=\"radio-studio\">Radio studio</button>\n            <button routerLink=\"weight-room\">Weight room</button>\n        </div>\n        <router-outlet></router-outlet>\n    </div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], Level2Component);
    return Level2Component;
}());
exports.Level2Component = Level2Component;
//# sourceMappingURL=level2.component.js.map