import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {Level2Component, AthleticsRoomComponent, BarbershopComponent, ClassroomComponent, FitnessRoomComponent, GameRoomComponent, RadioStudioComponent, WeightRoomComponent} from './index';

import {GuardLevel2Service} from '../../shared/guards/guard-level2.service';

export const levelRoutes: Routes = [
    {
        path: 'level2',
        component: Level2Component,
        canActivate: [GuardLevel2Service],
        canActivateChild: [GuardLevel2Service],
        children: [
            {
                path: '',
                redirectTo: 'athletics-room',
                pathMatch: 'full'
            },
            {
                path: 'athletics-room',
                component: AthleticsRoomComponent
            },
            {
                path: 'barbershop',
                component: BarbershopComponent
            },
            {
                path: 'classroom',
                component: ClassroomComponent
            },
            {
                path: 'fitness-room',
                component: FitnessRoomComponent
            },
            {
                path: 'game-room',
                component: GameRoomComponent
            },
            {
                path: 'radio-studio',
                component: RadioStudioComponent
            },
            {
                path: 'weight-room',
                component: WeightRoomComponent
            }
        ]
    }
]

export const routing: ModuleWithProviders = RouterModule.forChild(levelRoutes);