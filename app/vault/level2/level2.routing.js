"use strict";
var router_1 = require('@angular/router');
var index_1 = require('./index');
var guard_level2_service_1 = require('../../shared/guards/guard-level2.service');
exports.levelRoutes = [
    {
        path: 'level2',
        component: index_1.Level2Component,
        canActivate: [guard_level2_service_1.GuardLevel2Service],
        canActivateChild: [guard_level2_service_1.GuardLevel2Service],
        children: [
            {
                path: '',
                redirectTo: 'athletics-room',
                pathMatch: 'full'
            },
            {
                path: 'athletics-room',
                component: index_1.AthleticsRoomComponent
            },
            {
                path: 'barbershop',
                component: index_1.BarbershopComponent
            },
            {
                path: 'classroom',
                component: index_1.ClassroomComponent
            },
            {
                path: 'fitness-room',
                component: index_1.FitnessRoomComponent
            },
            {
                path: 'game-room',
                component: index_1.GameRoomComponent
            },
            {
                path: 'radio-studio',
                component: index_1.RadioStudioComponent
            },
            {
                path: 'weight-room',
                component: index_1.WeightRoomComponent
            }
        ]
    }
];
exports.routing = router_1.RouterModule.forChild(exports.levelRoutes);
//# sourceMappingURL=level2.routing.js.map