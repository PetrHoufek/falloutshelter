"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shared_module_1 = require('../../shared/shared.module');
var index_1 = require('./index');
var level2_routing_1 = require('./level2.routing');
var Level2Module = (function () {
    function Level2Module() {
    }
    Level2Module = __decorate([
        core_1.NgModule({
            imports: [shared_module_1.SharedModule, level2_routing_1.routing],
            declarations: [index_1.Level2Component, index_1.AthleticsRoomComponent, index_1.BarbershopComponent, index_1.ClassroomComponent, index_1.FitnessRoomComponent, index_1.GameRoomComponent, index_1.RadioStudioComponent, index_1.WeightRoomComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], Level2Module);
    return Level2Module;
}());
exports.Level2Module = Level2Module;
//# sourceMappingURL=level2.module.js.map