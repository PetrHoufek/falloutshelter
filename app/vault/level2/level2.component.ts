import {Component} from '@angular/core';

@Component({
    template:
    `
    <div>
        <div style="margin: 5px 0">
            Level 2
            <button routerLink="../level1">Up</button>
            <button routerLink="../level3">Down</button>
        </div>
        <div>
            <button routerLink="athletics-room">Athletics room</button>
            <button routerLink="barbershop">Barbershop</button>
            <button routerLink="classroom">Classroom</button>
            <button routerLink="fitness-room">Fitness room</button>
            <button routerLink="game-room">Game room</button>
            <button routerLink="radio-studio">Radio studio</button>
            <button routerLink="weight-room">Weight room</button>
        </div>
        <router-outlet></router-outlet>
    </div>
    `
})
export class Level2Component {

}