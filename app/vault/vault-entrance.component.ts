import {Component} from '@angular/core';

import {LoginService} from '../shared/services/login.service';

@Component({
    moduleId: module.id,
    templateUrl: './vault-entrance.component.html',
    styleUrls: ['./vault-entrance.component.css']
})
export class VaultEntranceComponent {

    constructor(private loginService: LoginService){
        
    }

}