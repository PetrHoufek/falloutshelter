"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var login_service_1 = require('../shared/services/login.service');
var user_service_1 = require('../shared/services/user.service');
var UpdateUserComponent = (function () {
    function UpdateUserComponent(loginService, userService) {
        this.loginService = loginService;
        this.userService = userService;
    }
    UpdateUserComponent.prototype.ngOnInit = function () {
        this.user = this.loginService.getUser();
        this.name = this.user.getName();
        this.age = this.user.getAge();
        this.job = this.user.getJob();
        this.password = this.user.getPassword();
    };
    UpdateUserComponent.prototype.updateUser = function () {
        this.userService.updateUser(this.age, this.job, this.password, this.user.getId());
    };
    UpdateUserComponent.prototype.hasPendingChanges = function () {
        if (this.age === this.loginService.getUser().getAge() && this.job === this.loginService.getUser().getJob() && this.password === this.loginService.getUser().getPassword()) {
            return false;
        }
        else {
            return true;
        }
    };
    UpdateUserComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: './update-user.component.html',
            styleUrls: ['./update-user.component.css']
        }), 
        __metadata('design:paramtypes', [login_service_1.LoginService, user_service_1.UserService])
    ], UpdateUserComponent);
    return UpdateUserComponent;
}());
exports.UpdateUserComponent = UpdateUserComponent;
//# sourceMappingURL=update-user.component.js.map