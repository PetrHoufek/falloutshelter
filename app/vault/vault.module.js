"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var shared_module_1 = require('../shared/shared.module');
var level1_module_1 = require('./level1/level1.module');
var level2_module_1 = require('./level2/level2.module');
var level3_module_1 = require('./level3/level3.module');
var index_1 = require('./index');
var login_service_1 = require('../shared/services/login.service');
var user_service_1 = require('../shared/services/user.service');
var guard_level1_service_1 = require('../shared/guards/guard-level1.service');
var guard_level2_service_1 = require('../shared/guards/guard-level2.service');
var guard_level3_service_1 = require('../shared/guards/guard-level3.service');
var guard_update_user_service_1 = require('../shared/guards/guard-update-user.service');
var guard_login_service_1 = require('../shared/guards/guard-login.service');
var vault_routing_1 = require('./vault.routing');
var VaultModule = (function () {
    function VaultModule() {
    }
    VaultModule = __decorate([
        core_1.NgModule({
            imports: [shared_module_1.SharedModule, forms_1.FormsModule, level1_module_1.Level1Module, level2_module_1.Level2Module, level3_module_1.Level3Module, vault_routing_1.routing],
            declarations: [index_1.VaultEntranceComponent, index_1.TerminalComponent, index_1.LoginComponent, index_1.RegisterComponent, index_1.UserListComponent, index_1.UpdateUserComponent, index_1.TerminalHomeComponent],
            providers: [login_service_1.LoginService, user_service_1.UserService, guard_level1_service_1.GuardLevel1Service, guard_level2_service_1.GuardLevel2Service, guard_level3_service_1.GuardLevel3Service, guard_update_user_service_1.GuardUpdateUserService, guard_login_service_1.GuardLoginService, http_1.HTTP_PROVIDERS]
        }), 
        __metadata('design:paramtypes', [])
    ], VaultModule);
    return VaultModule;
}());
exports.VaultModule = VaultModule;
//# sourceMappingURL=vault.module.js.map