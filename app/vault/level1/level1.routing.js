"use strict";
var router_1 = require('@angular/router');
var index_1 = require('./index');
var guard_level1_service_1 = require('../../shared/guards/guard-level1.service');
exports.levelRoutes = [
    {
        path: 'level1',
        canActivate: [guard_level1_service_1.GuardLevel1Service],
        component: index_1.Level1Component,
        children: [
            {
                path: '',
                redirectTo: 'diner',
                pathMatch: 'full'
            },
            {
                path: '',
                canActivateChild: [guard_level1_service_1.GuardLevel1Service],
                children: [
                    {
                        path: 'diner',
                        component: index_1.DinerComponent
                    },
                    {
                        path: 'lounge',
                        component: index_1.LoungeComponent
                    }
                ]
            },
            {
                path: 'living-quarters',
                component: index_1.LivingQuartersComponent
            }
        ]
    }
];
exports.routing = router_1.RouterModule.forChild(exports.levelRoutes);
//# sourceMappingURL=level1.routing.js.map