import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {Level1Component, DinerComponent, LivingQuartersComponent, LoungeComponent} from './index';

import {GuardLevel1Service} from '../../shared/guards/guard-level1.service';

export const levelRoutes: Routes = [
    {
        path: 'level1',
        canActivate: [GuardLevel1Service],
        component: Level1Component,
        children: [
            {
                path: '',
                redirectTo: 'diner',
                pathMatch: 'full'
            },
            {
                path: '',
                canActivateChild: [GuardLevel1Service],
                children: [
                    {
                        path: 'diner',
                        component: DinerComponent
                    },
                    {
                        path: 'lounge',
                        component: LoungeComponent
                    }
                ]
            },
            {
                path: 'living-quarters',
                component: LivingQuartersComponent
            }
        ]
    }
]

export const routing: ModuleWithProviders = RouterModule.forChild(levelRoutes);