"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./level1.component'));
__export(require('./diner.component'));
__export(require('./lounge.component'));
__export(require('./living-quarters.component'));
//# sourceMappingURL=index.js.map