"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Level1Component = (function () {
    function Level1Component() {
    }
    Level1Component = __decorate([
        core_1.Component({
            template: "\n    <div>\n        <div style=\"margin: 5px 0\">\n            Level 1\n            <button routerLink=\"../\">Up</button>\n            <button routerLink=\"../level2\" >Down</button>\n        </div>\n        <div>\n            <button routerLink=\"diner\">Diner</button>\n            <button routerLink=\"lounge\">Lounge</button>\n            <button routerLink=\"living-quarters\">Living quarters</button>\n        </div>\n        <br>\n        <router-outlet></router-outlet>\n    </div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], Level1Component);
    return Level1Component;
}());
exports.Level1Component = Level1Component;
//# sourceMappingURL=level1.component.js.map