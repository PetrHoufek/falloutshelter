"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var shared_module_1 = require('../../shared/shared.module');
var index_1 = require('./index');
var level1_routing_1 = require('./level1.routing');
var Level1Module = (function () {
    function Level1Module() {
    }
    Level1Module = __decorate([
        core_1.NgModule({
            imports: [shared_module_1.SharedModule, level1_routing_1.routing],
            declarations: [index_1.Level1Component, index_1.DinerComponent, index_1.LoungeComponent, index_1.LivingQuartersComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], Level1Module);
    return Level1Module;
}());
exports.Level1Module = Level1Module;
//# sourceMappingURL=level1.module.js.map