import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';

import {Level1Component, DinerComponent, LoungeComponent, LivingQuartersComponent} from './index';

import {routing} from './level1.routing';

@NgModule({
    imports: [SharedModule, routing],
    declarations: [Level1Component, DinerComponent, LoungeComponent, LivingQuartersComponent]
})
export class Level1Module {
    
}