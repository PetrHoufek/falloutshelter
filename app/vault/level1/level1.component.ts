import {Component} from '@angular/core';

@Component({
    template: 
    `
    <div>
        <div style="margin: 5px 0">
            Level 1
            <button routerLink="../">Up</button>
            <button routerLink="../level2" >Down</button>
        </div>
        <div>
            <button routerLink="diner">Diner</button>
            <button routerLink="lounge">Lounge</button>
            <button routerLink="living-quarters">Living quarters</button>
        </div>
        <br>
        <router-outlet></router-outlet>
    </div>
    `
})
export class Level1Component {

}