import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {User} from '../shared/services/user.service';

import {LoginService} from '../shared/services/login.service';
import {UserService} from '../shared/services/user.service';

@Component({
    moduleId: module.id,
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    user: User;

    password: string;

    constructor(private loginService: LoginService, private userService: UserService, private router: Router, private route: ActivatedRoute){

    }

    ngOnInit(){
        this.route.params.forEach((params: Params) => {
            this.user = this.userService.getUser(params['id']);
        })
    }

    login(){
        
        if(this.user.getPassword() == this.password){

            this.loginService.login(this.user);
            this.router.navigate(['../../../'], {relativeTo: this.route});

        }else{
            this.password = undefined;
        }

    }

}