import {Component} from '@angular/core';

@Component({
    template: '<router-outlet></router-outlet>',
    selector: 'my-app',
})
export class AppComponent {

}