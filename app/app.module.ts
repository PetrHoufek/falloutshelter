import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {VaultModule} from './vault/vault.module';

import {AppComponent} from './app.component';

import {routing} from './app.routing';

@NgModule({
    imports: [BrowserModule, VaultModule, routing],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
    
}