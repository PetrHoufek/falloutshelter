"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var login_service_1 = require('../services/login.service');
var GuardLevel1Service = (function () {
    function GuardLevel1Service(loginService, router, route) {
        this.loginService = loginService;
        this.router = router;
        this.route = route;
    }
    GuardLevel1Service.prototype.canActivate = function () {
        if (this.loginService.isLoggedIn()) {
            return true;
        }
        console.log("[GUARD1] In order to enter the vault, please login or register in the terminal");
        this.router.navigate(['../'], { relativeTo: this.route });
        return false;
    };
    GuardLevel1Service.prototype.canActivateChild = function () {
        if (this.loginService.getUser() && this.loginService.getUser().getJob() != 'unemployed') {
            return true;
        }
        console.log("[GUARD1] A lazy dweller who doesn't work, doesn't eat, go get a job!");
        this.router.navigate(['level1/living-quarters'], { relativeTo: this.route });
        return false;
    };
    GuardLevel1Service = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [login_service_1.LoginService, router_1.Router, router_1.ActivatedRoute])
    ], GuardLevel1Service);
    return GuardLevel1Service;
}());
exports.GuardLevel1Service = GuardLevel1Service;
//# sourceMappingURL=guard-level1.service.js.map