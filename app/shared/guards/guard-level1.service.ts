import {Injectable} from '@angular/core';
import {Router, ActivatedRoute, CanActivate, CanActivateChild} from '@angular/router';

import {LoginService} from '../services/login.service';

@Injectable()
export class GuardLevel1Service implements CanActivate, CanActivateChild{

    constructor(private loginService: LoginService, private router: Router, private route: ActivatedRoute){

    }

    canActivate(): boolean{
        
        if(this.loginService.isLoggedIn()){
            return true;
        }

        console.log("[GUARD1] In order to enter the vault, please login or register in the terminal")
        this.router.navigate(['../'], {relativeTo: this.route});

        return false;

    }

    canActivateChild(): boolean{

        if(this.loginService.getUser() && this.loginService.getUser().getJob() != 'unemployed'){
            return true;
        }

        console.log("[GUARD1] A lazy dweller who doesn't work, doesn't eat, go get a job!")
        this.router.navigate(['level1/living-quarters'], {relativeTo: this.route});

        return false;

    }

}