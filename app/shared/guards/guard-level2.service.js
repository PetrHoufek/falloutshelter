"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var login_service_1 = require('../../shared/services/login.service');
var router_1 = require('@angular/router');
var GuardLevel2Service = (function () {
    function GuardLevel2Service(router, loginService) {
        this.router = router;
        this.loginService = loginService;
    }
    GuardLevel2Service.prototype.canActivate = function () {
        if (this.loginService.isLoggedIn()) {
            return true;
        }
        console.log("[GUARD1] In order to enter the vault, please login or register in the terminal");
        this.router.navigate(['entrance']);
        return false;
    };
    GuardLevel2Service.prototype.canActivateChild = function () {
        if (this.loginService.getUser() && this.loginService.getUser().getJob() != 'unemployed') {
            return true;
        }
        console.log("[GUARD2] A lazy dweller who doesn't work, doesn't have to get any privileges, go get a job!");
        return false;
    };
    GuardLevel2Service = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [router_1.Router, login_service_1.LoginService])
    ], GuardLevel2Service);
    return GuardLevel2Service;
}());
exports.GuardLevel2Service = GuardLevel2Service;
//# sourceMappingURL=guard-level2.service.js.map