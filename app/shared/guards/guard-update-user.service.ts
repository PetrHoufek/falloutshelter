import {Injectable} from '@angular/core';

import {CanDeactivate} from '@angular/router';

import {UpdateUserComponent} from '../../vault/index';

export class GuardUpdateUserService implements CanDeactivate<UpdateUserComponent>{

    canDeactivate(target: UpdateUserComponent): boolean{
        if(target.hasPendingChanges()){
            if(window.confirm("Do you really want to cancel? All unsaved changes will be lost")){
                return true;
            }else{
                return false;
            }
        }
        return true;
    }

}