import {Injectable} from '@angular/core';
import {CanActivateChild} from '@angular/router';

import {LoginService} from '../../shared/services/login.service';

import {Router} from '@angular/router';

@Injectable()
export class GuardLevel2Service implements CanActivateChild {

    constructor(private router: Router, private loginService: LoginService){

    }

    canActivate(): boolean{
        
        if(this.loginService.isLoggedIn()){
            return true;
        }

        console.log("[GUARD1] In order to enter the vault, please login or register in the terminal")
        this.router.navigate(['entrance']);

        return false;

    }

    canActivateChild(): boolean{

        if(this.loginService.getUser() && this.loginService.getUser().getJob() != 'unemployed'){
            return true;
        }

        console.log("[GUARD2] A lazy dweller who doesn't work, doesn't have to get any privileges, go get a job!");

        return false;

    }

}