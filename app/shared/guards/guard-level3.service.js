"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var login_service_1 = require('../../shared/services/login.service');
var GuardLevel3Service = (function () {
    function GuardLevel3Service(router, loginService) {
        this.router = router;
        this.loginService = loginService;
    }
    GuardLevel3Service.prototype.canActivate = function () {
        if (this.loginService.isLoggedIn()) {
            return true;
        }
        console.log("[GUARD1] In order to enter the vault, please login or register in the terminal");
        this.router.navigate(['entrance']);
        return false;
    };
    GuardLevel3Service.prototype.canActivateChild = function (route, router) {
        if (this.loginService.getUser() && (route.url.toString() == 'lockers' || this.loginService.getUser().getJob() == route.url.toString() || this.loginService.getUser().getJob() == 'overseer-office')) {
            return true;
        }
        console.log("[GUARD3] You can't access [" + route.url.toString() + "] because you don't work there, your station is [" + this.loginService.getUser().getJob() + "]");
        return false;
    };
    GuardLevel3Service = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [router_1.Router, login_service_1.LoginService])
    ], GuardLevel3Service);
    return GuardLevel3Service;
}());
exports.GuardLevel3Service = GuardLevel3Service;
//# sourceMappingURL=guard-level3.service.js.map