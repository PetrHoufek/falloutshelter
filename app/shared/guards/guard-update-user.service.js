"use strict";
var GuardUpdateUserService = (function () {
    function GuardUpdateUserService() {
    }
    GuardUpdateUserService.prototype.canDeactivate = function (target) {
        if (target.hasPendingChanges()) {
            if (window.confirm("Do you really want to cancel? All unsaved changes will be lost")) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    };
    return GuardUpdateUserService;
}());
exports.GuardUpdateUserService = GuardUpdateUserService;
//# sourceMappingURL=guard-update-user.service.js.map