import {Injectable} from '@angular/core';
import {Router, ActivatedRoute, CanActivate} from '@angular/router';

import {LoginService} from '../services/login.service';

@Injectable()
export class GuardLoginService implements CanActivate{

    constructor(private loginService: LoginService, private router: Router, private route: ActivatedRoute){

    }

    canActivate(): boolean{

        if(this.loginService.isLoggedIn()){
            return true;
        }

        this.router.navigate(['../../'], {relativeTo: this.route});
        return false;

    }

}