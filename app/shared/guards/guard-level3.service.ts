import {Injectable} from '@angular/core';

import {Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import {LoginService} from '../../shared/services/login.service';

@Injectable()
export class GuardLevel3Service implements CanActivate, CanActivateChild {

    constructor(private router: Router, private loginService: LoginService){

    }

    canActivate(): boolean{
        
        if(this.loginService.isLoggedIn()){
            return true;
        }

        console.log("[GUARD1] In order to enter the vault, please login or register in the terminal")
        this.router.navigate(['entrance']);

        return false;

    }

    canActivateChild(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): boolean{
        
        if(this.loginService.getUser() && (route.url.toString() == 'lockers' || this.loginService.getUser().getJob() == route.url.toString() || this.loginService.getUser().getJob() == 'overseer-office')){
            return true;
        }

        
        console.log("[GUARD3] You can't access [" + route.url.toString() + "] because you don't work there, your station is [" + this.loginService.getUser().getJob() + "]");

        return false;

    }

}