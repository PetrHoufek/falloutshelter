"use strict";
var LoginService = (function () {
    function LoginService() {
    }
    LoginService.prototype.login = function (user) {
        this.loggedIn = true;
        this.user = user;
    };
    LoginService.prototype.updateUser = function (user) {
        if (this.loggedIn) {
            this.user = user;
        }
    };
    LoginService.prototype.logout = function () {
        this.loggedIn = false;
        this.user = undefined;
    };
    LoginService.prototype.isLoggedIn = function () {
        return this.loggedIn;
    };
    LoginService.prototype.getUser = function () {
        return this.user;
    };
    return LoginService;
}());
exports.LoginService = LoginService;
//# sourceMappingURL=login.service.js.map