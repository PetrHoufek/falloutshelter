"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var login_service_1 = require('./login.service');
require('rxjs/add/operator/toPromise');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/map');
require('rxjs/add/operator/switchMap');
var UserService = (function () {
    function UserService(http, loginService) {
        this.http = http;
        this.loginService = loginService;
        this.baseUrl = "http://localhost:8080/VaultServlet/";
        this.addUserPath = "addUser";
        this.removeUserPath = "removeUser";
        this.updateUserPath = "updateUser";
        this.getUsersPath = "getUsers";
        this.JOBS = [
            'living-quarters',
            'diner',
            'lounge',
            'weight-room',
            'athletics-room',
            'fitness-room',
            'game-room',
            'barbershop',
            'classroom',
            'radio-studio',
            'outfit-workshop',
            'power-generator',
            'water-treatment',
            'medbay',
            'science-lab',
            'weapon-workshop',
            'garden',
            'overseer-office',
            'storage-room',
            'armory',
            'water-purification',
            'nuclear-reactor',
            'unemployed'
        ];
        //when there is no database connection
        this.USERS = [];
    }
    UserService.prototype.addUser = function (name, age, gender, job, password) {
        var _this = this;
        var params = new http_1.URLSearchParams();
        params.append('name', name);
        params.append('age', age.toString());
        params.append('gender', gender);
        params.append('job', job);
        params.append('password', password);
        var id;
        this.http.post(this.baseUrl + this.addUserPath, params.toString()).toPromise().then(function (e) {
            _this.getUsers();
            id = e.text();
            _this.loginService.login(new User(id, name, age, gender, job, password));
        }).catch(function (f) {
            console.log('User registration failed!');
        });
    };
    UserService.prototype.removeUser = function (id) {
        var _this = this;
        var params = new http_1.URLSearchParams();
        params.append('id', id.toString());
        return this.http.post(this.baseUrl + this.removeUserPath, params.toString()).toPromise().then(function (e) {
            _this.loginService.logout();
            _this.getUsers();
        }).catch(function (f) {
            console.log('User removal failed!');
        });
    };
    UserService.prototype.getUsers = function (id) {
        var _this = this;
        this.http.post(this.baseUrl + this.getUsersPath, '').toPromise().then(function (e) {
            _this.USERS = JSON.parse(e.text()).map(function (user) { return (new User(user.id, user.name, user.age, user.gender, user.job, user.password)); });
            _this.loginService.updateUser(_this.getUser(id));
        });
    };
    UserService.prototype.getRetrievedUsers = function () {
        return this.USERS;
    };
    UserService.prototype.getUser = function (id) {
        for (var i = 0; i < this.USERS.length; i++) {
            if (this.USERS[i].getId() == id) {
                return this.USERS[i];
            }
        }
    };
    UserService.prototype.updateUser = function (age, job, password, id) {
        var _this = this;
        var params = new http_1.URLSearchParams();
        params.append('age', age.toString());
        params.append('job', job);
        params.append('password', password);
        params.append('id', id.toString());
        this.http.post(this.baseUrl + this.updateUserPath, params).toPromise().then(function (e) {
            _this.getUsers(id);
        }).catch(function (f) {
            console.log('User update failed!');
        });
    };
    UserService.prototype.getJobs = function () {
        return this.JOBS;
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, login_service_1.LoginService])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
var User = (function () {
    function User(id, name, age, gender, job, password) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.job = job;
        this.password = password;
    }
    User.prototype.getId = function () {
        return this.id;
    };
    User.prototype.setId = function (id) {
        this.id = id;
    };
    User.prototype.getName = function () {
        return this.name;
    };
    User.prototype.getAge = function () {
        return this.age;
    };
    User.prototype.getGender = function () {
        return this.gender;
    };
    User.prototype.getJob = function () {
        return this.job;
    };
    User.prototype.setJob = function (job) {
        this.job = job;
    };
    User.prototype.getPassword = function () {
        return this.password;
    };
    User.prototype.setPassword = function (password) {
        this.password = password;
    };
    User.prototype.toString = function () {
        return "Name: " + this.name + " Age: " + this.age + " Gender: " + this.gender + " Job: " + this.job;
    };
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.service.js.map