import {User} from './user.service';

export class LoginService {

    user: User;

    loggedIn: boolean;

    login(user: User){

        this.loggedIn = true;
        this.user = user;

    }

    updateUser(user: User){
        if(this.loggedIn){
            this.user = user;
        }
    }

    logout(){

        this.loggedIn = false;
        this.user = undefined;

    }

    isLoggedIn(): boolean{
        return this.loggedIn;
    }

    getUser(): User{
        return this.user;
    }

}