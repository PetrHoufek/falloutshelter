import {Injectable} from '@angular/core';

import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {LoginService} from './login.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class UserService{

    baseUrl: string = "http://localhost:8080/VaultServlet/";
    addUserPath: string = "addUser";
    removeUserPath: string = "removeUser";
    updateUserPath: string = "updateUser";
    getUsersPath: string = "getUsers"

    JOBS: string[] = [
        'living-quarters',
        'diner',
        'lounge',
        'weight-room',
        'athletics-room',
        'fitness-room',
        'game-room',
        'barbershop',
        'classroom',
        'radio-studio',
        'outfit-workshop',
        'power-generator',
        'water-treatment',
        'medbay',
        'science-lab',
        'weapon-workshop',
        'garden',
        'overseer-office',
        'storage-room',
        'armory',
        'water-purification',
        'nuclear-reactor',
        'unemployed'
    ];

    //when there is no database connection
    USERS: User[] = [
        //new User(0, 'Petr', 24, 'male', 'overseer-office', 'motorbike'),
        //new User(1, 'Jakub', 30, 'male', 'nuclear-reactor', 'kayak'),
        //new User(2, 'Michal', 21, 'male', 'power-generator', 'lisa'),
        //new User(3, 'Hana', 20, 'female', 'unemployed', 'flower')
    ];

    constructor(
        private http: Http,
        private loginService: LoginService
    ){

    }

    addUser(name: string, age: number,  gender: string,  job: string, password: string){

        let params = new URLSearchParams();

        params.append('name', name);
        params.append('age', age.toString());
        params.append('gender', gender);
        params.append('job', job);
        params.append('password', password);

        let id;

        this.http.post(this.baseUrl + this.addUserPath, params.toString()).toPromise().then(e => {
            this.getUsers();
            id = e.text();
            this.loginService.login(new User(id, name, age, gender, job, password));
        }).catch(f => {
            console.log('User registration failed!');
        })

    }

    removeUser(id: number): Promise<any>{

        let params:URLSearchParams = new URLSearchParams();

        params.append('id', id.toString());

        return this.http.post(this.baseUrl + this.removeUserPath, params.toString()).toPromise().then(e => {
            this.loginService.logout();
            this.getUsers();
        }).catch(f => {
            console.log('User removal failed!');
        })

    }

    getUsers(id?: number){
        this.http.post(this.baseUrl + this.getUsersPath, '').toPromise().then(e => {
            this.USERS = JSON.parse(e.text()).map(user => (new User(user.id, user.name, user.age, user.gender, user.job, user.password)));
            this.loginService.updateUser(this.getUser(id));
        })
    }

    getRetrievedUsers(): User[]{
        return this.USERS;
    }

    getUser(id: number): User{
        
        for(let i = 0; i < this.USERS.length; i++){
            
            if(this.USERS[i].getId() == id){
                return this.USERS[i];
            }

        }

    }

    updateUser(age: number, job: string, password: string, id: number){

        let params: URLSearchParams = new URLSearchParams();

        params.append('age', age.toString());
        params.append('job', job);
        params.append('password', password);
        params.append('id', id.toString());

        this.http.post(this.baseUrl + this.updateUserPath, params).toPromise().then(e => {
            this.getUsers(id);
        }).catch(f => {
            console.log('User update failed!');
        })

    }

    getJobs(): string[]{
        return this.JOBS;
    }

}

export class User {

    constructor(private id: number, private name: string, private age: number,  private gender: string,  private job: string, private password: string){

    }

    getId(): number{
        return this.id;
    }

    setId(id: number){
        this.id = id;
    }

    getName(): string{
        return this.name;
    }

    getAge(): number{
        return this.age;
    }

    getGender(): string{
        return this.gender;
    }

    getJob(): string{
        return this.job;
    }

    setJob(job: string){
        this.job = job;
    }

    getPassword(): string{
        return this.password;
    }

    setPassword(password: string){
        this.password = password;
    }

    toString(): string{
        return "Name: " + this.name + " Age: " + this.age + " Gender: " + this.gender + " Job: " + this.job;
    }

}